package ru.t1.rleonov.tm.component;

import ru.t1.rleonov.tm.api.ICommandController;
import ru.t1.rleonov.tm.api.ICommandRepository;
import ru.t1.rleonov.tm.api.ICommandService;
import ru.t1.rleonov.tm.constant.TerminalArguments;
import ru.t1.rleonov.tm.constant.TerminalCommands;
import ru.t1.rleonov.tm.controller.CommandController;
import ru.t1.rleonov.tm.repository.CommandRepository;
import ru.t1.rleonov.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private void processCommands() {
        System.out.println("***WELCOME TO TASK-MANAGER***");
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private void processArguments(final String[] args) {
        final String arg = args[0];
        processArgument(arg);
    }

    private void processCommand(final String command) {
        if (command == null) {
            commandController.showErrorCommand();
            return;
        }
        switch (command) {
            case TerminalCommands.ABOUT:
                commandController.showAbout();
                break;
            case TerminalCommands.VERSION:
                commandController.showVersion();
                break;
            case TerminalCommands.HELP:
                commandController.showHelp();
                break;
            case TerminalCommands.INFO:
                commandController.showInfo();
                break;
            case TerminalCommands.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalCommands.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalCommands.EXIT:
                exitApplication();
                break;
            default:
                commandController.showErrorCommand();
        }
    }

    private void processArgument(final String arg) {
        switch (arg) {
            case TerminalArguments.ABOUT:
                commandController.showAbout();
                break;
            case TerminalArguments.VERSION:
                commandController.showVersion();
                break;
            case TerminalArguments.HELP:
                commandController.showHelp();
                break;
            case TerminalArguments.INFO:
                commandController.showInfo();
                break;
            case TerminalArguments.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalArguments.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    private static void exitApplication() {
        System.exit(0);
    }

    public void runApplication(final String[] args) {
        if (args == null || args.length == 0) {
            processCommands();
            return;
        }
        processArguments(args);
    }

}
